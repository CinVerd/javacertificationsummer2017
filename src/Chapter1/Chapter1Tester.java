package Chapter1; //Note, default package does exist and is allowed

import java.util.*;
import java.sql.*;

//import java.util.Date;
//import java.sql.Date; Compile error, can't set both to take precedence!


/**
 * General messing around with chapter 1 concepts
 * Created by dskerritt on 11/05/2017.
 */
public class Chapter1Tester {

    public Chapter1Tester(){
        //valid constructor
    }

    public void Chapter1Tester(){
        //NOT A CONSTRUCTOR
        //NOTE RETURN TYPE IS SET
    }


    public static void main(String args[]){
        //Date date; //Compile error, need to specifically import the date we want to use
        java.sql.Date sqlDate;  //fine, explicitly declaring type
    }
}
