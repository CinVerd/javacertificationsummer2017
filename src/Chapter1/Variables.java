package Chapter1;

/**
 * Different possibilities about naming and using variables
 * Created by dskerritt on 11/05/2017.
 */
public class Variables {

    //THESE ARE ALL INSTANCE/CLASS VARIABLES

    //if static, then class variable (obviously, otherwise would be instance, right?
    static boolean myBoolean; //class
    char myChar;    //== '\u0000' (NUL) //instance
    int intArray[] = {1,2,3,4,5};

    //int value = null; cant assign null to a primitive
    String myString = null;
    //primitives have no functions associated with them

    String s1 ="string", s2 = "whatever", s3, s4 = "four";
    //int num, String value; //wrong!

    int _validName;
    int $validName;
    int clAss;  //valid, but why do this?

    /**********************Default Initialization*************************/

    /*public int notValid(){
        int y =10;
        int x;
        int reply = x+y;//X is NOT instantiated yet
        return reply;
    }

    //compiler knows chance x will not be instantiated, throws compile error
    public int notValidAlso(boolean check){
        int y =10;
        int x;
        if(check){
           x=1;
        }else{

        }
        int reply = x+y;//X may not be instantiated yet
        return reply;
    }*/

    /*VARIABLE SCOPE*/
}
