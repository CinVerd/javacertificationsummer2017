package Chapter1;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dskerritt on 19/06/2017.
 */
public class DestroyingObjects {

    //this MIGHT get called, will NOT get called twice
    //Program might exit before any need to call
    protected void finalize() {
        System.out.println("Finalize called");
    }

    public static void main(String[] args){
        System.gc();    //not guaranteed to run, just suggests to Java to run it

    }
}


class Finalizer{
    private static List objects = new ArrayList();
    protected void finalize() {
        objects.add(this);  //Wrong on so many levels
        //This is now not eligible for garbage collection, because there's a reference to it, so finalize is stopped
        //But it will never be called again.
    }
}
