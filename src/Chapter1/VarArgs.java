package Chapter1;

/**
 * Created by dskerritt on 19/06/2017.
 */
public class VarArgs {
    public static void main(String... $n){
        //above in place of array, called varargs
        System.out.println($n[0] + $n[1]);
    }
}
