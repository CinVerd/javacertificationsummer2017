package Chapter1;

/**
 * Multiple classes can be defined in one file, at MOST ONE allowed to be public,
 * Created by dskerritt on 19/06/2017.
 */
public class Meerkat {

    public Meerkat(){
        tail myTail = new tail();
        System.out.println("Tail length is " + myTail.getLength());
    }


    class tail{
        int length;
        {length = 5;}//instance initiializer
        public int getLength(){
            return length;
        }
    }


}
//no access modifier allowed here
class paw{{
    Meerkat animal = new Meerkat();

    //animal.tail = new animal.tail();  //tail not static, can't access it, requires instance of meerkat first
}}