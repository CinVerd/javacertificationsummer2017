package Chapter1;

/**
 * Created by dskerritt on 11/05/2017.
 */
public class Numbers {

    static int oct = 017; //uses 0 at start
    static int hex = 0xFF; //uses 0x at start
    static int bin = 0b10; //uses 0b at start

    double number = 1_000.00__00;    //not at start of number, between decimal places, or at end, so between digits

    //float y = 2.1;//assumed to be double unless has f at end (same as long)
    long number2 = 10;

    //long myNumber = 111111111111111111;

    public static void main(String[] args) {
        System.out.println(oct);
        System.out.println(hex);
        System.out.println(bin);
    }
}
