package Chapter1;

/**
 * Standard stuff about instantiating classes
 * Created by dskerritt on 11/05/2017.
 */
public class Chick {
    //8bits
    byte number = 127;  //Only one need to know, -128 to 127
    //byte number = (byte)129;  //Only one need to know, -128 to 127
    //long max =3123456788; treats as int
    //long max =3123456788; //compile error, number too large
    long max =3123456788L; //lowercase l works too, but looks too much like a 1.


    //Note no return type
    //Default constructors automatically created
    //Runs once all instance initializer blocks AND field inits have run
    public Chick(){
        System.out.println("Constructor");
    }

    public static void main(String[] args){
        {System.out.println("Before instantiate chick");}   //also valid
        Chick ch = new Chick();
        System.out.println("After instantiate chick");
    }


    //Order is important here as elsewhere, can't reference things that have not been defined yet.
    {System.out.println("Instance Initializer!");}//Runs before Constructor print.
    private String name1 = "Name";  //runs after
    {System.out.println(name1);}
    {name1 = "Nametexthaschanged";}
    {System.out.println(name1);}

}
