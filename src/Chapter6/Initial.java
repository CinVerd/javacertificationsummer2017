package Chapter6;

/**
 * Created by cinve on 23/06/2017.
 */
public class Initial {
    public static void main(String[] args) throws Exception {
        try{
            throw new RuntimeException();   //Goes to catch statement
        }catch (RuntimeException e){
            throw new RuntimeException(); //finally block WILL run after catch block
            //System.out.println("Never printed");
        }finally{
            throw new Exception(); //this is the exception thrown, exception in catch block is ignored.
        }
    }
}
