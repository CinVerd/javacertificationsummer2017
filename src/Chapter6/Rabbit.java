package Chapter6;

/**
 * Created by cinve on 23/06/2017.
 */
public class Rabbit {

    public void bad(){
       /* try{
            eatCarrot();
        }catch(NoMoreCarrotsException e){       //Error here because nomorecarrots never thrown by eatCarrot
            System.out.println("Sad rabbit");
        }*/
    }

    public void good() throws NoMoreCarrotsException{
        eatCarrot();
    }

    private static void eatCarrot(){}

    public static void main(String[] args) {
        Bunny bun = new Bunny();
        bun.hop(); //hop on bunny doesn't throw one

        Hopper hoppy = new Bunny();
        //Calling method on super, now we need to handle it
        try {
            hoppy.hop();
        } catch (NoMoreCarrotsException e) {
            e.printStackTrace();
        }


    }
}

class Hopper{
    public void hop() throws NoMoreCarrotsException { throw new NoMoreCarrotsException();}
}

class Bunny extends Hopper{
    public void hop() {}
}


