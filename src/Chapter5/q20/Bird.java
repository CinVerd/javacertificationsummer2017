package Chapter5.q20;

/**
 * Created by cinve on 23/06/2017.
 */
public abstract class Bird {

    private void fly() { System.out.println("Bird is flying."); } //this is private, can only be hidden, NOT overridden

    public static void main(String[] args) {
        Bird bird = new Pelican();
        bird.fly();
    }
}

class Pelican extends Bird {
    protected void fly() { System.out.println("Pelican is flying"); }
}
