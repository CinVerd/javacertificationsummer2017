package Chapter5;

/**
 * Created by dskerritt on 22/06/2017.
 */
public interface DefaultMethods {

    //way of maintaining backwards compatability
    //Only within interface
    //Not assumed  final static final or abstract
    //Is assumed public
    public default double getTemperature(){
        return 10.0;
    }

    static  double getStaticTemp(){
        return 5.0;
    }

}
