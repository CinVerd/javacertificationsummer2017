package Chapter5.MethodOverriding;

/**
 * Created by cinve on 23/06/2017.
 */
public class Gorilla extends Animal{
/*    protected String getName(){ //not allowed, more restricted access
        return "Gorilla";
    }*/

    public String getName(){ //not allowed, more restricted access
        return "Gorilla";
    }
}
