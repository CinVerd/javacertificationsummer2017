package Chapter5.MethodOverriding;

/**
 * Created by cinve on 23/06/2017.
 */
public class ZooKeeper {

    public static void main(String[] args) {
        Animal animal = new Gorilla();

        System.out.println(animal.getName());   //Contradiction here, if gorilla getName was protected.
        //Polymorphism means that the public getName in Animal is overridden with the protected getName in gorilla, which should not be available to this class
        //But, because it's being referenced as in instance of animal, with public access, it should be allowed.
        //Contradiction occurs here.
    }
}
