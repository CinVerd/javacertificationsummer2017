package Chapter5;

/**
 *
 * If these methods were static (hidden) instead, then output would be false, true
 * As we're overriding, then method is....overridden, and child one is called in both cases, outputting true, true
 *
 * Created by dskerritt on 22/06/2017.
 */
public class Kangaroo extends Marsupial{

    public boolean isBiped(){
        return true;
    }

    public void getKangarooDescription(){
        System.out.println("Kangaroo walks two legs: " + isBiped());
    }

    public static void main(String[] args) {
        Kangaroo joey = new Kangaroo();
        joey.getMarsupialDescription();
        joey.getKangarooDescription();
    }
}

class Marsupial{
    public boolean isBiped(){
        return false;   //This is replaced at runtime with call to child class
    }

    public void getMarsupialDescription(){
        System.out.println("Marsupial walks two legs: " + isBiped()); //isBiped is the child class function
    }
}
