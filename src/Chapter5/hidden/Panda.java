package Chapter5.hidden;

/**
 * Created by dskerritt on 22/06/2017.
 */
public class Panda extends Bear {

    public static void eat(){
        System.out.println("Panda bear is chewing.");
    }

    public void move(){
        //super refers to an instance of the class
        super.eat();
        System.out.println("Moving");
    }

    public static void main(String[] args) {
        Panda.eat();
        Panda p = new Panda();
        //this.super.eat();
    }
}
