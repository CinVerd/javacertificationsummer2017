package Chapter5;

/**
 * Created by dskerritt on 22/06/2017.
 */
public class Initial {

    Initial(){
        //System.out.println("Invalid"); //Compiler error
        //Java will automatically insert this into compiled code if not called
        super();    //calling super constructor, in this case the constructor for object

    }

    public static void main(String[] args) {

    }
}
