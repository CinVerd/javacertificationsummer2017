package Chapter3;

import javax.sound.midi.Soundbank;

/**
 * Created by dskerritt on 20/06/2017.
 */
public class StringMethods {

    //Note that string indexing starts at 0
    public static void main(String[] args) {
        stringComp();
        String myString = "animals";

        System.out.println(myString.length());  //outputs 7

        System.out.println(myString.charAt(2)); //outputs i, will throw exception if too big

        //Finds first index that matches value, from index i
        System.out.println(myString.indexOf('a'));  //0
        System.out.println(myString.indexOf("al"));  //4
        System.out.println(myString.indexOf('a', 4));  //4
        System.out.println(myString.indexOf("al", 5));  //-1, not found

        //Substring(begin index, end index EXCLUSIVE)
        System.out.println(myString.substring(3));
        System.out.println(myString.substring(myString.indexOf('m')));  //mals
        System.out.println(myString.substring(3,4)); //m
        System.out.println(myString.substring(3,7)); //mals (no need to have second parameter in this case)

        System.out.println(myString.substring(3,3)); //empty string
        System.out.println(myString.substring(3,2)); //exception
        System.out.println(myString.substring(3,8)); //

        //Cases - remember immutability!
        System.out.println(myString.toLowerCase()); //animals
        System.out.println(myString.toUpperCase()); //ANIMALS

        //Equals and EqualsIgnoreCase
        System.out.println("ABC".equals("ABC"));    //true
        System.out.println("ABC".equals("AbC"));    //false
        System.out.println("ABC".equalsIgnoreCase("AbC"));    //true

        //Startswith Endswithv (Takes a string)
        System.out.println("ABC".startsWith("A")); //true
        System.out.println("ABC".startsWith("a")); //false
        System.out.println("ABC".endsWith("C")); //true
        //System.out.println("ABC".endsWith('C')); //invalid!

        //contains
        System.out.println("abc".contains("b"));    //true
        System.out.println("abc".contains("B"));    //false

        //replace replace(char, char) or replace (char sequence, char sequence)
        System.out.println("abcabc".replace('a', 'A')); //AbcAbc
        System.out.println("abcabc".replace("a", "A")); //AbcAbc

        //trim
        //clears whitespace from start and end of string
        //Removes \t (Tab), \n. \r

        //Don't forget, methods can be chained.
        String result = "AniMaL     ".trim().toLowerCase().replace('a', 'A');
        System.out.println(result);
    }

    private static void what(){
        String alpha = "";
        for(char current = 'a'; current <='z'; current++){
            alpha += current;
            System.out.println(alpha);
        }
    }

    private static void StringBuilding(){
        //StringBuilder is mutable
        StringBuilder alpha = new StringBuilder();
        for(char current = 'a'; current <='z'; current++){
            alpha.append(current);
            System.out.println(alpha);
        }

        StringBuilder sb = new StringBuilder("start");
        sb.append("middle");
        StringBuilder same = sb.append("end");  //sb AND same point to the same object

        //Creating stringbuilders
        StringBuilder sb1 = new StringBuilder();
        StringBuilder sb2 = new StringBuilder("animal");
        StringBuilder sb3 = new StringBuilder(10); //capacity of string builder. Will automatically increase if needed.
    }

    private static void StringBuilderMethods(){
        //charAt, indexOf, length, substring all work as they do for String
        //note that substring will return immutable string

        //append - 10 different methods, takes strings, characters, ints, booleans etc.

        //insert
        StringBuilder sb = new StringBuilder("animals");
        sb.insert(7, "-"); //will throw exception if index is too big, 7 counts as end of sequence, so is fine.
        sb.insert(0, "-");
        sb.insert(4, "-");
        System.out.println(sb); // -ani-mals

        //delete, deleteCharAt
        StringBuilder sb1 = new StringBuilder("abcdef");
        sb1.delete(1, 3);   //adef
        sb1.deleteCharAt(5);    //Exception, we've modified string so index at 5 no longer exists.

        //reverse() - pretty obvious

        //toString() - same
    }

    public static void inequality(){
        StringBuilder one = new StringBuilder();
        StringBuilder two = new StringBuilder();
        StringBuilder three = one.append("a");
        System.out.println(one == two); //false
        System.out.println(one == three); //true

        String x = "Hello World";
        String y = "Hello World"; //if made new string object here, would not be the same
        System.out.println(x == y); //true, due to string pooling
        String z = " Hellow World".trim();
        System.out.println(x == z); //false
        System.out.println(x.equals(z)); //true

    }

    public static void stringComp(){
        String s1 = new String("Hello World"); //create brand new object, used only by this
        String s2 = "Hello World";
        System.out.println(s1 == s2);
    }
}
