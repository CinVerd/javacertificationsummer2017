package Chapter3;

/**
 * Created by dskerritt on 19/06/2017.
 */
public class Strings {

    public static void main(String[] args) {

    }

    private static void concat(){
        //If both operands are numbers, + means addition
        //If one or both operands are strings, + means concat
        //Evaluated left to right

        System.out.println(1 + 2); //2
        System.out.println("a" + "b"); //ab
        System.out.println("a" + "b" + 3); //ab3
        System.out.println(1 + 2 + "c"); //3c

        //What follows below is the technical definition of "bullshit"
        int three = 3;
        String four = "4";

        System.out.println(1 + 2 + three + four); //outputs.... 64

        String s = "1";
        s += "2";
        s += 3;
        System.out.println(s);  //123
    }

    private static void immutability(){
        String s1 = "1";
        String s2 = s1.concat("2");
        s2.concat("3"); //RETURNS a new string, does NOT modify s2.
        System.out.println(s2);

    }

    private static void stringPool(){
        String name = "Fluffy";
        String name2 = new String("Fluffy"); //Does NOT use string pool, makes new string. Don't do it, but know is allowed.
    }
}
