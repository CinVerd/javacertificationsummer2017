package Chapter3;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;

/**
 * Created by dskerritt on 20/06/2017.
 */
public class DateAndTime {

    public static void main(String[] args) {
        modifyingDates();
    }

    //CANNOT CONSTRUCT THESE
    //These are immutable
    private static void basics(){
        System.out.println(LocalDate.now());
        System.out.println(LocalTime.now());
        System.out.println(LocalDateTime.now());

        LocalDate date1 = LocalDate.of(2015, Month.JANUARY, 20);
        LocalDate date2 = LocalDate.of(2015, 1, 20);

        LocalTime time1 = LocalTime.of(6, 15);
        LocalTime time2 = LocalTime.of(6, 15, 30);
        LocalTime time3 = LocalTime.of(6, 15, 30, 200);

        LocalDateTime dateTime1 = LocalDateTime.of(2015, Month.JANUARY, 20, 6, 15, 30);
        LocalDateTime dateTime2 = LocalDateTime.of(date1, time1);
    }

    private static void modifyingDates(){
        //date plusMonths, plusYears, minusMonths etc
        LocalDate date1 = LocalDate.now();
        date1 = date1.plusMonths(-5);
        date1 = date1.minusMonths(-5);
        System.out.println(date1);

        LocalDateTime dateTime1 = LocalDateTime.now();
        dateTime1 = dateTime1.minusHours(5);
        //seconds etc start showing once start using (if had made time of 5:30, for example.
        //Be aware of what methods can be called on time vs date
    }

    private static void periods(){
        Period period = Period.ofMonths(2);
        LocalDate date1 = LocalDate.now();
        date1.plus(period);
        //note, CANNOT chain these methods
        Period wrong = Period.ofMonths(1).ofDays(2);    //period of two days returned
        Duration time = Duration.ofMinutes(1);  //not on exam
        Period what = Period.of(1,2,3);//NB
    }

    private static void formatting(){
        //in package java.time.format

        LocalDate date = LocalDate.of(2020, Month.JANUARY, 20);
        LocalTime time = LocalTime.of(11, 12, 34);

        LocalDateTime dateTime = LocalDateTime.of(date, time);
        System.out.println(date.format(DateTimeFormatter.ISO_LOCAL_DATE));
        System.out.println(time.format(DateTimeFormatter.ISO_LOCAL_TIME));
        System.out.println(dateTime.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));

        DateTimeFormatter shortDateTime = DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT);

        System.out.println(shortDateTime.format(dateTime)); //shows date
        System.out.println(shortDateTime.format(date)); //shows date
        System.out.println(shortDateTime.format(time)); //exception

        //Short and medium formats can show on exam

        //can parse string to date/time
        //if anything goes wrong with parsing, always runtime exception (if pattern doesn't match eg)
        DateTimeFormatter f = DateTimeFormatter.ofPattern("MM dd yyyy");
        LocalDate date1 = LocalDate.parse("01 02 2015", f);
    }
}
