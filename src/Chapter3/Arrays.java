package Chapter3;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by dskerritt on 20/06/2017.
 */
public class Arrays {

    public static void main(String[] args) {
        arrayLists();
    }

    private static void basicArrays(){
        int[] numbers1 = new int[3]; //Values are set to default, in this case, 0.
        //int[] numbers2 = new int[]{42, 55, 99};
        int[] numbers2 = {42, 55, 99};  //shorter way to write, also acceptable (anonymous array, as don't specify type and size)

        //[] can exist before or after name
        int[] numAnimals;
        int [] numAnimals2;
        int numAnimals3[];
        int numAnimals4 [];

        int[] ids, types;   //creates two arrays
        int ids1[], types1; //creates one array ids1, one int types1


    }

    private static void confusingArrays(){
        String[] strings = {"stringvalue"};
        Object[] objects = strings; //automatically cast "up", string is a subtype of object
        String[] againStrings = (String[]) objects; //new object?
        //againStrings[0] = new StringBuilder(); //error
        objects[0] = new StringBuilder();   //runtime exception! StringBuilder is type of object, but this variable actually references a string[]

        int[] var1[];   //This is a 2D array.
        int[] var2[], var3[][];   //This is a 2D array, followed by a 3D array.

        int[][] differentSizes = {{1, 4}, {3}, {9,8,7}};

        int[][] args = new int[4][]; //can define sizes of sub arrays later
        args[0] = new int[5];
        args[1] = new int[3];

    }

    private static void arrayLists(){
        ArrayList list1 = new ArrayList();
        ArrayList list2 = new ArrayList(10);
        ArrayList list3 = new ArrayList(list2); //copy array list

        ArrayList<String> list4 = new ArrayList<String>();
        ArrayList<String> list5 = new ArrayList<>();

        list5.toString();   //pretty print of contents

        ArrayList list = new ArrayList();   //will allow any object
        list.add("hawk");
        list.add(Boolean.TRUE);

        ArrayList<String> safer = new ArrayList<>();
        safer.add("sparrow");
        //safer.add(1);

        ArrayList<String> birds = new ArrayList<>();
        birds.add("hawk");
        birds.add(1, "robin");
        birds.add(0, "blue jay");
        birds.add(1, "cardinal"); //does NOT overwrite what's already there
        System.out.println(birds.toString());

        //remove
        //either returns boolean indicating object removed, or returns actual object removed

        //set - changes an leement
        birds.set(0, "duck"); //will throw error if index too large

        //isEmpty size() - does NOT include capacity, only actual values.

        //clear - self explanatory

        //contains - pass in object, boolean returned if object exists, method calls "equals" on each element.

        //equals - sees if have same elements in same order
    }

    private static void wrappers(){
        //can use to put primitives into arraylist
        //Not actually needed to be done, because of autoboxing
        int number1 = Integer.parseInt("123");
        Integer number2 = Integer.valueOf("123");   //returns object

        ArrayList<Integer> heights = new ArrayList<>();
        heights.add(null);
        int h = heights.get(0); //attempting to unbox null, throws npe, because trying to call method on null

        //be careful of using remove, because it will remove element at that index, not the actual integer (need to pass integer object if want to remove specific number)
    }

    private static void convertingListToArray(){
        List<String> list = new ArrayList<>();
        list.add("Hawk");
        list.add("Robin");
        Object[] objectArray = list.toArray();  //Returns of type object

        System.out.println(objectArray.length); //prints 2

        String[] stringArray = list.toArray(new String[0]); //specifies type of array, need to pass in type we want, setting size 0 means exact size created
        String[] stringArray1 = list.toArray(new String[1]); //size not big enough here, java auto creates larger array
        System.out.println(stringArray.length); //prints 2
    }

    private static void convertArrayToList(){
        String[] array = {"hawk", "robin"};
        List<String> list = java.util.Arrays.asList(array);
        System.out.println(list.size());
        list.set(1, "test");
        array[0] = "new";
        for (String b: array) System.out.println(b + " "); // new test
        list.remove(1); //unsupported exception thrown

        //sorting
        Collections.sort(list);

    }

}
