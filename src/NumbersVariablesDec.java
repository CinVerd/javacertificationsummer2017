/**
 * Created by dskerritt on 10/07/2017.
 */
public class NumbersVariablesDec {

    int my_Number = 4;
    int _OtherNumber = 5;
    int $ = 1;
    int __$____$_$_$ = 1;
    int Class = 1;
    //int class = 1;

    //Value assignment here, the names are valid
    int numberOne = 017;
    int numberTwo = 0xFF;
    int numberThree = 0b10;
    int numberFour = 1_0_0;
    //int numberFive = _100;
    //String one = 1;
    String myNumber = "4";
}
