package reflection;

import java.lang.reflect.Field;

/**
 * //Modify string? Same object?
 * Created by cinve on 31/01/2018.
 */
public class Tester {

    public String first = "Java";
    public String second = "Java";

    public static void main(String[] args) throws IllegalAccessException, NoSuchFieldException {

       Tester tester = new Tester();

        tester.stringMess();
        //breakPD();
    }

    public void stringMess() throws NoSuchFieldException, IllegalAccessException {

        /*Field firstField = this.getClass().getField("first");
        firstField.setAccessible(true);

        firstField.set(this, "Kava");*/

        Class<String> type = String.class;
        Field value = type.getDeclaredField("value");
        value.setAccessible(true);

        //http://www.javacreed.com/how-to-modify-a-string-without-creating-a-new-one/
        char[] valueArray = (char[]) value.get(first); //using Field that exists on String objects called value
        valueArray[0] = 'K';

        System.out.println("First is " + first + ", second is " + second + ". Are they equal objects?");
        System.out.println(first == second);
    }

    public static void breakPD() throws IllegalAccessException {
        PrivateDetails pd = new PrivateDetails();

        Field[] fields = pd.getClass().getDeclaredFields();

        System.out.println("Printing Fields");
        for(Field f: fields){
            System.out.println(f.getName());
            System.out.println(f.toGenericString());
            f.setAccessible(true);
            //f.getType().;
            String value = (String)f.get(pd);
            System.out.println("Value is " + value);

        }

        System.out.println();
    }
}
