package reflection;

/**
 * Created by cinve on 31/01/2018.
 */
public class PrivateDetails {

    private String name = "David";
    private String lastName = "Skerritt";

    public String getName() {
        return name;
    }

    public String getLastName() {
        return lastName;
    }
}
