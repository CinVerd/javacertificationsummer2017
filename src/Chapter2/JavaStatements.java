package Chapter2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by dskerritt on 19/06/2017.
 */
public class JavaStatements {

    //if statement
    //if then else
    //ternary operator
    //type psvm to generate main method
    public static void main(String[] args) {
        int y = 10;
        System.out.println((y>5) ? 21: "Zebra");    //both converted to string
        //int x = (y < 91) ? 9: "Horse";   //error

        //IMPORTANT`
        //Ternary operators and side effects
        //loops(4);
        String myString;
        //System.out.println(myString);   //local variables ALWAYS need to be init'd.

        loops(4);
    }

    /**
     * Ternary operators only execute piece of code that needs to be run
     * If true, left side is run, if false, right side is run.
     */
    public static void sideEffects(){
        int y = 1;
        int z = 1;
        final int x = y < 10 ? y++ : z++;
        System.out.println(y + "," + z);//Outputs 2,1

        int y1 = 1;
        int z1 = 1;
        final int x1 = y1 >= 10 ? y1++ : z1++;
        System.out.println(y1 + "," + z1);//Outputs 1,2
    }

    public static void switchStatement(String input) {
        //Supports primitives, wrapper classes, strings

        //Be careful to take note of existence of break keywords
        //Returns can be used also

        //byte, short, char, int, String, enum
        switch(input) { //brackets required
            case "a":
                //whatever
                break;
            case "b":
                //whatever else
                break;
            default://may appear anywhere
                break;
        }
    }

    public static void loops(int input){
        /*while(input < 5){
            //do something
        }*/
        while(input < 5)
            System.out.println(input++); //loops once if 4 is passed in
        System.out.println("Second Text");


        //Guaranteed run at least once.
        //Brackets not required for single line
        do{

        }while(input<5);

        do
            System.out.println("Do while");
            //System.out.println("Do while");
         while(input<5);

        //For statement
        //Basic vs enhanced
        for(int i=10; i >5; i++){   //again, optional for single line

        }

        for(; ;){   //ALL parts are optional, loops infinitely

        }
        /*for(;;)
            input++;*/
    }

    /**
     * Examples of odd for loops.
     */
    public static void weirdForLoops(int input){
        /*for(;;){
            //loops infinitely
        }*/

        //Multiple terms in for statement
        int x = 0;
        for(long y = 0, z = 4; x<5 && y<10; x++, y++){

        }

        /*for(long y = 0, x = 4; x<5 && y<10; x++, y++){    //Will not compile, declaring variable x, it already exists in scope (could assign new value)

        }*/
        //Variables only have scope where declared! Could not access y, eg, outside loop.
        /*for(long y = 0, int z = 4; x<5 && y<10; x++, y++){ //Will not compile, cannot have different types declared (similar to variable declaration anywhere)

        }*/
    }

    public static void forEach(){
        //for(datatype instance: collection){}  //right hand side must implement iterable, left hand is instance of variable
        final String[] names = new String[3];
        names[0] = "Lisa";
        names[1] = "Kevin";
        names[2] = "Roger"; //if these were never populated, code would still compile.

        //Datatype must match, names must be iterable, name is variable name changed in each loop
        for( String name : names) {
            System.out.print(name + ", ");
        }

        //out of scope for exam, but interesting to note
        List<Integer> values = new ArrayList<Integer>();
        List<Integer> values2 = new ArrayList<Integer>(Arrays.asList(1,2,3,4)); //Other way to instantiate
        values.add(1);
        values.add(2);
        values.add(3);
        values.add(4);

        for(java.util.Iterator<Integer> i = values.iterator(); i.hasNext();){
            int value = i.next();
            System.out.println(value + ",");
        }
    }

    public static void confusingWhile(){
        int x = 20;
        while(x>0){
            do{
                x -=2;
            }while (x > 5);
            x--;
            System.out.println(x+"\t");//outputs 3  0
        }
    }

    public static void optionalLabel(){
        //NOT IN EXAM
        MAIN_LOOP: for(int i=0; i<5; i++){
            System.out.println("WTF " + i);
            if(i == 3){
                break MAIN_LOOP;
            }else if( i == 2){
                continue MAIN_LOOP; //Label not needed, goes back to boolean logic for for loop, determines if goes again
                // Continue ends current iteration of loop_
            }
        }
    }

    public static void optionalLabel2(){
        //NOT IN EXAM
        MAIN_LOOP: for(int i=0; i<5; i++){
            System.out.println("Outer Loop " + i);
            INNER_LOOP: for( int j=0; j<5; j++){
                if(i == 3){
                    break INNER_LOOP; //would function the same if just calling 'break'
                }
                if(j == 2){
                    break MAIN_LOOP;
                }
            }
        }
    }

}
