package Chapter2;

import java.io.File;

/**
 * Created by dskerritt on 19/06/2017.
 */
public class RelationalOps {

    public static void main(String...args){
        //< <= > >=
        //promotion happens here too
        //a instanceof b  -not in exam, used for seeting if a is instance of class/subclass/class implements interface
        boolean answer = true ^ false;  //Exclusive OR operator

    }

    private static void shortCircuit(String message){
        if(message != null && message.length() >2){ //if only one &, NPE thrown.
            System.out.println(message.charAt(1));
        }
        //can only use on objects, numeric primitives, and booleans

        File y = new File("text.txt");
        File x = new File("text.txt");
        File z = x;

        System.out.println(x == y); //false
        System.out.println(x == z); //true, points to same file
    }
}
