package Chapter2;

/**
 * Created by dskerritt on 19/06/2017.
 */
public class BinaryOps {

    public static void main (String[] args){
        test();
        //arith operators apply to any primitive, except boolean
        //+ and += may be applied to string.
        //% modulo, gets remainder
        System.out.println(10%3);   //outputs 1 (floor rounded, decimals are truncated)
        //modulus also works on floating point and negative numbers, not needed for exam

        //Numeric Promotion
        int x = 5;
        long y =5L;
        long z = x*y;   //result is of type of largest variable type, x is promoted

        //byte, short, char are auto promoted to int

        short num1 = 5;
        short num2 = 4;
        int num3 = num1 + num2;
        short num4 = (short) (num1 + num2);
        //short num4 = num1 +  promoted to int

        //cant do mad stuff like int x = !5;
        //boolean y = -true
        //boolean t = (boolean)1; //Can't cast int as boolean
        int myVariable = 4;
        //int myVariable = ++ not valid
        System.out.println(++myVariable);//Changed before
        System.out.println(myVariable++);//Changed after

    }

    public static void why(){
        int x = 3;
        int y = ++x * 5 / x-- + --x; //Order of operation is important.
        System.out.println("x is " + x);
        System.out.println("y is " + y);

        int number = (int)1.0;

    }

    public static void test(){
        int i = 1;
        while(i < 5){
            i = i++;
            System.out.println("Message");
        }

    }
}
