package Chapter2;

/**
 * Created by dskerritt on 19/06/2017.
 */
public class CompoundOps {

    public static void main(String...args){
        int x=2, z=3;

        x *=z;  //if x not instantiated, compile error

        long a = 5;
        int b = 2;
        //b = b * a;
        b *= a;//implicit casting back to int

    }

    public static void more(){
        long x = 5;
        long y = (x=3);

        //x AND y both equal to 3
    }
}
