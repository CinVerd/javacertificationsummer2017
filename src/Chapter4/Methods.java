package Chapter4;

import java.util.ArrayList;

/**
 * Created by dskerritt on 21/06/2017.
 */
public class Methods {
    private static final ArrayList<String> values = new ArrayList<>();
    //Static initializer
    static {
        //can initially set value of final here
    }

    public static void main(String[] args) {
        Koala.main(new String[0]);
        Koala k = new Koala();
        //k is instance of object, if calling static method on instance of object, doesn't matter if null.
        System.out.println(k.count); //prints 0
        k = null;
        System.out.println(k.count); //prints 0
    }



    public static void finals(){
        values.add("Changed");  //not changing what values references, just changing something in it. values still same.
    }


}

class Koala {
    public static int count = 0;
    public static void main(String[] args){
        System.out.println(count);
    }

}
