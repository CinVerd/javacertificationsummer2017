package Chapter4;

import java.util.Arrays;
import java.util.List;

import static java.util.Arrays.asList;
//import static java.util.Arrays;   //error
//note can not import two statics with same name


/**
 * Created by dskerritt on 21/06/2017.
 */
public class StaticImports {

    public static void main(String[] args) {
        Arrays.asList("one");   //still need to import directly
        List<Integer> myList = asList(1,2,3,4);
    }
}
