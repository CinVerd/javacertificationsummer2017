package Chapter4.lambdas;

/**
 * Created by dskerritt on 22/06/2017.
 */
public interface CheckTrait {

    boolean test(Animal a);
}
