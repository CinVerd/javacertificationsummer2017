package Chapter4.lambdas;

/**
 * Created by dskerritt on 22/06/2017.
 */
public class CheckIfHopper implements CheckTrait {

    //bypassing this function call, returning value directly!
    public boolean test(Animal a) {
        return a.canHop();
    }

}
