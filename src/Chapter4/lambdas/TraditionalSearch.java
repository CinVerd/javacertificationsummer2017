package Chapter4.lambdas;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

/**
 * Created by dskerritt on 22/06/2017.
 */
public class TraditionalSearch {

    public static void main(String[] args) {
        List<Animal> animals = new ArrayList<Animal>();

        animals.add(new Animal("fish", false, true));
        animals.add(new Animal("kangaroo", true, false));
        animals.add(new Animal("rabbit", true, false));
        animals.add(new Animal("turtle", false, true));

        print(animals, new CheckIfHopper());

        //passing in lambda when expecting CheckTrait. That interface method test takes an animal, so java infers animal, expects boolean back
        //Only works with ONE function in interface, hence it can infer.
        //bypassing the function call in CheckTrait, returning value directly!

        //Second parameter is expecting something that takes type Animal, returns boolean. Animal has several methods that return boolean, hence can use here.
        printPredicate(animals, a -> a.canHop());
        //printPredicate(animals, a -> a.canSwim());

        //Lambda syntax
        //Don't parenthesis if only one paramter, type not stated
        //Single statement can omit braces, as normal. If put braces in, then need semicolon and return statement
        //printPredicate(animals, (Animal a) -> { return a.canSwim();});
        bunnyExample();
        //ArrayList has a removeIf function that takes a predicate, only one need to know for exam
    }

    private static void print(List<Animal> animals, CheckTrait checker){
        for(Animal animal : animals){
            if(checker.test(animal))
                System.out.println(animal + " ");
        }
        System.out.println();
    }

    //Predicate is generic interface, saves us having to write our own.
    private static void printPredicate(List<Animal> animals, Predicate<Animal> checker){
        for(Animal animal : animals){
            if(checker.test(animal))    //this checker.test call is actually the function that was passed in
                System.out.println(animal + " ");
        }
        System.out.println();
    }

    private static void bunnyExample(){
        List<String> bunnies = new ArrayList<>();
        bunnies.add("long ear");
        bunnies.add("floppy");
        bunnies.add("hoppy");
        System.out.println(bunnies);
        //Predicate that takes a string, returns a boolean
        bunnies.removeIf(s -> s.charAt(0) != 'h');
        System.out.println(bunnies);
    }

    public Integer myMethod(){
        return null;    //null Integer object
        //return null;
    }

    public int myMethod2(){
        //return null; //can't have null primitive
        return 0;
    }
}
