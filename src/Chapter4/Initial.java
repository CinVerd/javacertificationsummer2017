package Chapter4;

/**
 * Created by dskerritt on 21/06/2017.
 */
public class Initial {

    public static void main(String[] args) {
        walk1(1);
        walk1(1, 2);
        walk1(1, 2,3);  //array is created
        walk1(1, new int[] {4,5});
        walk1(1, null); //null is not empty array, npe thrown when accessing .length
    }

    //public private protected and [default] are access modifiers.
    //protected same package or subclasses
    //[default] same package

    //static abstract final are optional ones
    //Optional specifiers BEFORE return type
    void thisIsValid(){}

    void exceptions() throws IllegalArgumentException, InterruptedException{

    }

    //varargs
    public static void walk1(int start, int... nums){
        System.out.println(nums.length);
    }

    //note: read about protected access modifiers again

}
