package Chapter4;

/**
 * Created by dskerritt on 21/06/2017.
 */
public class Hamster {

    public Hamster(int weight){
        //last place to do final assignement in, apart from declaring and initializer
        //MUST BE FIRST NON COMMENTED LINE IN CONSTRUCTOR(makes sense, right?)
        this(weight, "brown");  //Note: use this in order to reference other constructor
        System.out.println("Small Constructor");
        new Hamster(weight, "brown");   //valid, but makes new object
        //Hamster(weight, "brown"); //wrong
    }

    public Hamster(int weight, String colour){
        System.out.println("Big Constructor");
    }
}
