package Chapter4;

/**
 * Created by dskerritt on 21/06/2017.
 */
public class PassingData {

    public static void main(String[] args) {

    }

    public static void initial(){
        String name = "Webby";
        speak(name);
        System.out.println(name); //Webby
    }

    private static void speak(String name) {
        name = "Sparky";
    }


    //Important
    public static void builder(){
        StringBuilder name = new StringBuilder();
        speak2(name);
        System.out.println(name); //Webby
    }

    //Variable s is a COPY of the reference variable that was passed in, but it still points to the same OBJECT
    public static void speak2(StringBuilder s){
        s.append("Webby");
    }

    //Autoboxing
    //Whichever you call these with, either numMiles(1) or numMiles(Integer.valueOf(1))
    //public void fly(int numMiles){}
    //public void fly(Integer numMiles){}

    //Reference Types
    public void fly(String s){
        System.out.println("string ");
    }

    public void fly(Object o){
        System.out.println("object");
    }

    public void callFly(){
        fly("test");
        fly(5); //gets autoboxed, as no int or Integer, goes to object parameter instead.
    }
}
