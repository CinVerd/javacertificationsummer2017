package other;

/**
 *
 * Only time method not overridden is when EXPLICIT call to parent.
 * Created by cinve on 31/01/2018.
 */
public class regularClass extends absClass{

    public static void main(String[] args) {
        regularClass reg = new regularClass(); //try with abstract class type
        reg.printDetails(); //should be subclass
        reg.callAbs();

        System.out.println("\nReference is to super.");
        absClass abs = reg;
        //should be abs details, regular
        abs.printDetails(); //not an explicit call to parent class method, still calling overridden, object is still of type regular, only reference has changed
        abs.callMeMaybe();

        regularClass otherRef  = (regularClass) abs;

        otherRef.printDetails();
        otherRef.callAbs();


    }


    public void printDetails(){
        System.out.println("Regular details");

    }

    public void callAbs(){
        super.printDetails(); //EXPLICIT call to parent class calls that actual function
        super.callMeMaybe(); //Should be Regular Details - overridden
    }
}
